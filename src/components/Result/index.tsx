import React from 'react'
import style from './style.module.scss'

interface Route {
  path?: Array<any>,
  cost?: string
}

type Props = {
  children?: any,
  route: Route
}

const Result = ({children, route}:Props) => (
  <div className={style.Result}>
    {children}

    {(route && route.path ?
      <div className={style.route}>

        {route.path.map((stop, i) => (
          <div key={i} className={style.stop}>
            <span>{stop}</span>
          </div>
        ))}

        <div className={style.cost}>
          <span>
            {route.cost}
          </span>
        </div>

      </div>
      :
      <div>No route found</div>
      )
    }
  </div>
)

Result.defaultProps = {
  children: '',
  route: []
};

export default Result