import React from 'react'
import style from './style.module.scss'

type Props = {
  children?: any,
  routes: Array<any>,
  selected: string,
  setRoute: Function,
  isDisabled?: string
}

const Locations = ({children, routes, selected, setRoute, isDisabled}:Props) => (
  <div className={style.Locations}>
    {children}

    <div>
      {routes.map((route, i) => (
          <button key={i}
            className={route.name === selected ? style.selected : ''}
            data-name={route.name}
            disabled={route.name === isDisabled ? true : false}
            onClick={() => setRoute(route.name)} >
            {route.name}
          </button>
        ))
      }
    </div>
  </div>
)

Locations.defaultProps = {
  children: '',
  routes: [],
  selected: false,
  setRoute: null,
  isDisabled: false
};

export default Locations