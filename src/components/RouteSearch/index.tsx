import React from 'react'
import Graph from 'node-dijkstra'

import Locations from '../Locations';
import Result from '../Result';

import style from './style.module.scss'

type Props = {}
type State = {
  isLoaded: boolean,
  routes: Array<any>,
  routeGraph: any,
  selectedStart: string,
  selectedEnd: string,
  shortestRoute: any
}

class RouteFinder extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      isLoaded: false,
      routes: [],
      routeGraph: null,
      selectedStart: '',
      selectedEnd: '',
      shortestRoute: []
    }
  }

  componentDidMount = () => {
    this.getGraphData()
  }

  // Retrieve routes from JSON file
  getGraphData = () => {
    fetch("./data/routes.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            routes: result
          });
          this.createGraph()
        },
        (error) => {
          this.setState({
            isLoaded: true,
          });
          console.log(error)
        }
      )
  }

  // Create Graph object from routes
  createGraph () {
    const routeGraph = new Graph()

    this.state.routes.map((route, i) => (
      routeGraph.addNode(
        route.name,
        route.edges
      )
    ))

    this.setState(
      {routeGraph: routeGraph}
    )
  }

  // Find shortest route and retrieve distance
  getRoute () {
    if(this.state.routeGraph && this.state.selectedStart && this.state.selectedEnd) {
      let result = this.state.routeGraph.shortestPath(this.state.selectedStart, this.state.selectedEnd, { cost: true })
      this.setState({
        shortestRoute: result
      });
    } else {
      console.log('no graph');
    }
  }

  // Set the starting point
  setRouteStart = (start: string) => {
    this.setState({
      selectedStart: start,
      selectedEnd: '',
      shortestRoute: []
    })
  }

  // Set the end point
  setRouteEnd = (end: string) => {
    this.setState({
      selectedEnd: end
    },this.getRoute)
  }

  render() {
    return (
      <div className={style.RouteSearch}>
        <h1>Route Finder</h1>

        {/* Set Start Point */}
        <Locations 
          routes={this.state.routes} 
          selected={this.state.selectedStart} 
          setRoute={this.setRouteStart}>
          <h2>Select route start</h2>
        </Locations>
        
        {/* Set End Point */}
        {this.state.selectedStart &&
          <Locations
            routes={this.state.routes} 
            selected={this.state.selectedEnd}
            setRoute={this.setRouteEnd}
            isDisabled={this.state.selectedStart}>
            <h2>Select Route End</h2>
          </Locations>
        }
      
        {/* Results */}
        {this.state.shortestRoute.path && 
          <Result route={this.state.shortestRoute}>
            <h2>Shortest Route</h2>
          </Result>
        }
      </div>
    )
  }
}

export default RouteFinder