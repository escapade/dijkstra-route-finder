import React from 'react';
import './App.css';

import RouteFinder from './components/RouteSearch/index';

const App: React.FC = () => {
  return (
    <div className="App">
      <RouteFinder />
    </div>
  );
}

export default App;
