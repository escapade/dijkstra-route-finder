## Dijkstra Route Finder - Max Hoy

### About
React Typescript App for finding the shortest route using the Dijkstra algorithm.

### Todos
- Add tests to the components
- See the distances between the nodes in the shortest route.

### Run
'yarn install && yarn start'